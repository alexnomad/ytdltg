FROM rust:latest as builder
WORKDIR /usr/src/ytdltg
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y openssl ca-certificates && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/ytdltg /usr/local/bin/ytdltg
COPY --from=builder /usr/src/ytdltg/telegram-bot-api /user/local/telegram-bot-api
CMD ["ytdltg"]
