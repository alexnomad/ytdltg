mod commands;
mod error;
mod logger;
mod prelude;

use std::path::Path;

use log::{debug, info, warn};
use teloxide::{filter_command, prelude::*, types::InputFile};
use youtube_dl::YoutubeDl;

pub type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;

#[tokio::main]
async fn main() {
    logger::setup_logging().unwrap();
    let _ = dotenv::dotenv();
    check_for_ytdlp().await;

    info!("Starting the local Telegram Bot API");

    let api_id = std::env::var("TELEGRAM_API_ID").expect("TELEGRAM_API_ID is not set");
    let api_hash = std::env::var("TELEGRAM_API_HASH").expect("TELEGRAM_API_HASH is not set");

    let mut api_path = Path::new("/usr/local/telegram-bot-api");
    if !api_path.exists() {
        warn!(
            "Telegram api not found in {} trying local",
            api_path.display()
        );
        api_path = Path::new("./telegram-bot-api");
        if !api_path.exists() {
            panic!("Telegram api not found!");
        }
    }

    std::process::Command::new(api_path)
        .args(["--api-id", &api_id, "--api-hash", &api_hash])
        .current_dir(Path::new("./telegram-bot-api"))
        .spawn()
        .unwrap();

    log::info!("Starting the bot!");
    let url = reqwest::Url::parse("http://0.0.0.0:8081").unwrap();
    let bot = Bot::from_env().set_api_url(url);

    let command_handler =
        filter_command::<commands::Command, _>().endpoint(commands::handle_command);
    let message_handler = Update::filter_message()
        .branch(command_handler)
        .branch(dptree::endpoint(default_responce));

    Dispatcher::builder(bot, message_handler)
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}

async fn check_for_ytdlp() {
    info!("Checking for ytdlp");
    let ytdlp_path = Path::new("./yt-dlp/");
    if ytdlp_path.exists() {
        info!("ytdlp found!");
        return;
    }

    info!("ytdlp not found! Downloading");
    youtube_dl::download_yt_dlp(ytdlp_path).await.unwrap();
}

async fn default_responce(bot: Bot, msg: Message) -> HandlerResult {
    // 1: Download video to the server
    let progress_message = bot
        .send_message(msg.chat.id, "Downloading the video...")
        .await?;

    let directory = Path::new("./videos/");
    let video_rid: u16 = rand::random();
    let url = msg.text().ok_or(error::HandleError::NoText)?;
    let download_result = YoutubeDl::new(url)
        .youtube_dl_path("./yt-dlp/yt-dlp")
        .format("mp4")
        .output_template(format!("{video_rid}.%(ext)s"))
        .download_to_async(directory)
        .await;

    match download_result {
        Ok(_) => {
            bot.edit_message_text(
                progress_message.chat.id,
                progress_message.id,
                "Downloaded the video from YT, uploading it to telegram",
            )
            .await?;
            // 2: Send the video to the user
            let str_path = format!("./videos/{video_rid}.mp4");
            let path = Path::new(&str_path);
            debug!("{path:#?}");
            if let Err(e) = bot.send_video(msg.chat.id, InputFile::file(path)).await {
                bot.edit_message_text(
                    progress_message.chat.id,
                    progress_message.id,
                    "There was an error uploading your video to Telegram",
                )
                .await?;
                std::fs::remove_file(path)?;
                return Err(Box::new(e));
            }
            bot.edit_message_text(
                progress_message.chat.id,
                progress_message.id,
                "Here's your video!",
            )
            .await?;
            // 3. Delete the video from the server
            std::fs::remove_file(path)?;
        }
        Err(e) => {
            log::error!("{e}");
            let message = format!("Download failed! Additional info: {e}");
            bot.edit_message_text(msg.chat.id, progress_message.id, message)
                .await?;
        }
    }
    Ok(())
}
