#[derive(thiserror::Error, Debug)]
pub enum HandleError {
    #[error("No text in a message")]
    NoText,
}
