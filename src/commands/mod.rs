use crate::prelude::*;
use teloxide::prelude::*;

use teloxide::utils::command::BotCommands;

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "Supported commands:")]
pub enum Command {
    #[command(description = "basic command")]
    Start,
}

pub async fn handle_command(bot: Bot, msg: Message, cmd: Command) -> HandlerResult {
    match cmd {
        Command::Start => {
            bot.send_message(
                msg.chat.id,
                "Send me a link to a YouTube video and I will download it!",
            )
            .await?;
        }
    }
    Ok(())
}
